from abc import ABC, abstractmethod

from receipt_raw import ReceiptRaw

class DataCollectionInterface:

    @abstractmethod
    def retrieve(self) -> ReceiptRaw:
        pass

    @abstractmethod
    def store(self):
        pass