import pandas as pd 
import numpy as np
import itertools as it
import coicop_classifier
import logging

# util functions for tuning

def prepare_gridsearch(params):
    '''
    Transform dict of parameters to dict of all combinations of parameters 
    
    :input params: dict of one or more parameters
    :returns: dict of the parameter setting for the grid search. 
    Keys are run identification numbers, starting at 0
    '''
    d_comb = {}

    if params == None:
        raise ValueError("Dict of params is empty")
    elif len(params) == 0:
        raise ValueError("Dict of params is empty")
    else:
        # make sure each item of dict is list of at least one element
        params_grid = params.copy()
        for k, v in params.items():
            if isinstance(v, list) == False:
                if isinstance(v, np.ndarray):
                    params_grid[k] = v.tolist()
                else:
                    params_grid[k] = [v]

        # expand parameters
        pnames = sorted(params_grid)
        combinations = list(it.product(*(params_grid[p] for p in pnames)))
        logging.debug("Number of iterations grid search:", len(combinations))

        d_comb = {}
        for i in range(len(combinations)):
            d = {}
            for j in range(len(combinations[i])):
                d[pnames[j]] = combinations[i][j]
            d_comb[i] = d
        d_comb

    return d_comb

def check_prefix_params(params, type='model'):
    """ Check if parameters start with correct prefix
    :param type (str): parameters for tuning a model or feature extractor
    :returns: True if all parameters correspond to the type
    """

    if type not in ['model', 'feature']:
        raise ValueError("Unknown type of parameters.")

    coicopClassifier = coicop_classifier.CoicopClassifier(
        "input", 
        "output",
        "coicop_models/", 
        "coicop_output/",
        set_split=0.8
    )

    for key in params.keys():
        if type == 'model':
            if key.startswith(tuple(coicopClassifier.dict_clf.keys())) == False:
                return False
        if type == 'feature':
            if key.startswith(tuple(coicopClassifier.dict_f_extract.keys())) == False:
                return False
    
    return True

def postprocess_results(results):
    """ Select and sort results of grid search
    :param results: dict with results and parameters
    """

    res = pd.DataFrame(results)
    filter_col = [col for col in res.columns if col.startswith('param')]
    filter_col += ['mean_test_score', 'std_test_score', 'rank_test_score']
    res = res[filter_col]
    res.sort_values('rank_test_score', ascending=True, inplace=True)
    logging.debug(res.head(20))
    return res
