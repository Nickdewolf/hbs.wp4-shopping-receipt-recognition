import cv2


class ReceiptRaw():
    """ 
    Used to store the rgb and gray_scale image of a receipt.
    By default the image used in ocr is the gray-scale image.
    """

    def __init__(self, rgb_image, gray_image):
        self.rgb_image = rgb_image
        self.gray_image = gray_image

    @property
    def ocr_image(self):
        return self.gray_image

    @classmethod
    def from_file(cls, input_url:str):
        """Creates a ReceiptRaw object based on a given path to an image.

        Args:
            input_url (str): Path to an image file.

        Returns:
            ReceiptRaw: object containing the rgb and gray-scale image.
        """
        rgb_image = cv2.imread(input_url)
        gray_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2GRAY)
        return cls(rgb_image, gray_image)
    
    