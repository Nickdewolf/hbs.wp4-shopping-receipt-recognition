""" Contains general helper functions for the NLP matching
Written by Nick de Wolf and Lanthao Benedikt
"""

from difflib import get_close_matches
import os

#nltk.download('punkt')
from nltk.tokenize import word_tokenize
from nltk.util import ngrams
import logging
import re

def list_files_in_folder(data_folder, extensions):
    """List all JPEG files in input folder "datadir" specify above and return list of file names.

    Args:
        data_folder (string): Path to the data_folder to look for files.
        extensions (list): List of strings with the extensions to look for.

    Returns:
        list: List of strings with paths to files
    """
    filenames = []
    for found_file in os.listdir(data_folder):
        if any(found_file.endswith(ext) for ext in extensions):
            filenames.append(data_folder + '/' + found_file)
    return filenames

def get_ngrams(line, max_n):
    """Make ngrams.

    Args:
        line (string): String to be turned into a list of n-grams.
        max_n (int): Max size for N in N-grams.

    Returns:
        list: List of strings representing the various n-grams in line.
    """
    ngram_list = []
    for n in range(1, max_n):
        n_grams = ngrams(word_tokenize(line), n)
        ngram_list = ngram_list + [' '.join(grams) for grams in n_grams]
    return ngram_list


# 
def fuzzy_match(pattern_list, lines, n_levels=1, max_acc_only=True, strip_digits=True):
    """Apply fuzzy matching between a list of patterns and a list of strings.

    Find words that closely resemble one of the patterns in a list of patterns
    Does not need to be an exact match. Two levels of dictionary supported

    Args:
        pattern_list (list): List of patters/strings to look for
        lines (list): List of strings in which to look for the patterns
        n_levels (int, optional): How deep to look. Defaults to 1.
        max_acc_only (boolean, optional): Only allow entries with the highest accuracy value. Defaults to True.
        strip_digits (boolean, optional): Strip digits from the pattern (regex is not influenced)

    Returns:
        (list, list, list): Lists of line_index, accuracy scores, and the method by which it was found.
    """
    found = []  # Default value if no pattern found
    idx = []
    accuracies = []
    logging.debug('Pattern list:')
    logging.debug(pattern_list)
    for int_accuracy in range(10, 7, -1):
        accuracy = int_accuracy / 10.0
        for i in range(0, len(lines)):
            logging.debug('Line %d: %s. Accuracy=%f' % (i, lines[i], accuracy))
            # Remove digits
            if strip_digits:
                line = ''.join([char for char in lines[i] if not char.isdigit()])
            # Make word ngrams list
            my_ngrams = get_ngrams(line, 4)
            if n_levels == 1:
                for pattern in pattern_list:
                    # Fuzzy match by keyword (better!) or exact match by pattern
                    if get_close_matches(pattern, my_ngrams, 1, accuracy) or re.search(pattern, lines[i]):
                        # if not i in idx:  # if index not already in set
                        idx.insert(0, i)
                        accuracies.insert(0, accuracy)
                        found.insert(0, pattern)
                        logging.debug('\n\tLine index=%d, pattern: %s\n' % (i, pattern))
            elif n_levels == 2:
                for pattern, spellings in pattern_list:
                    for spelling in spellings:
                        if get_close_matches(spelling, my_ngrams, 1, accuracy) or re.search(spelling, lines[i]):
                            # if not i in idx:  # if index not already in set
                            idx.insert(0, i)
                            accuracies.insert(0, accuracy)
                            found.insert(0, pattern)
                            logging.debug('\n\tLine index=%d, pattern: %s, found: %s\n' % (i, spelling, pattern))
    # Return only indices where the accuracy is highest
    if max_acc_only == 1:
        idx_list = [i for i, j in enumerate(accuracies) if j == max(accuracies)]
        idx = [idx[i] for i in idx_list]
        accuracies = [accuracies[i] for i in idx_list]
        found = [found[i] for i in idx_list]
    return [idx, accuracies, found]
