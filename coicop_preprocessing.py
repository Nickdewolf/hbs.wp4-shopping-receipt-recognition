""" Contains functions used in preprocessing data involved with classifying COICOP codes.
Written by Nick de Wolf
"""

import re
from collections import Counter
import numpy as np
import pandas as pd
import logging

class CoicopPreprocessing:

    @staticmethod
    def remove_dots(coicop_string):
        """Converts a string representation of a coicop into an integer representation

        Transforms the dotted notation to the CPI notation. 
        Only the numbers for the first 4 "dots" are maintained.
        
        Usage:
            remove_dots("12.1.2.1") -> 12121

        Args:
            coicop_string (string): dotted- String representation of a COICOP code

        Returns:
            integer: Integer representation of the COICOP code
        """
        s = "".join(coicop_string.split(".")[:-1])
        if s[0] == "0":
            s = s[1:]
        if len(s) < 5:
            s += "0"
        if s[-1] != "0":
            s += "0"

        return int(s)
    
    @staticmethod
    def add_dots(coicop_int):
        """Transforms the CPI format (5-6 digit) towards the notation using the dots.

        Writes a coicop code in integer representation, into a 4 digit dotted-string notation.

        Usage:
            add_dots(12121) -> "12.1.2.1"

        Args:
            coicop_int (integer): Integer representation of a COICOP code

        Returns:
            string: Dotted-string representation of the COICOP code
        """
        coicop_string = str(coicop_int)
        if len(coicop_string) == 5:
            coicop_string = "0"+coicop_string
        return "{0:s}{1:s}.{2:s}.{3:s}.{4:s}".format(*list(coicop_string))
    
    @staticmethod
    def tokenizer(sentence):
        """Tokenizes a given sentences following some standard NLP steps.

        Args:
            sentence (string): The to be tokenized sentence

        Returns:
            string: Tokenized sentence
        """
        # Default shop names that only add noise in dutch.
        common_words_list = ["ah", "jumbo", "kruidvat", "aldi", "lidl"]

        sentence = sentence.lower()
        sentence = re.sub(r'(\d*,?\.?)\d+\s*(((kilo|mili)?\s*grams?)|gr|(k|m)?g)', "UNIT_SOLID", sentence)
        sentence = re.sub(r'(\d*,?\.?)\d+\s*((mili|centi)?\s*liter(s)?|((c|m)?l))', "UNIT_LIQUID", sentence)
        # Might mess up product names with numbers,
        # but this substitution usually removes errors with receipt prices in the product name
        sentence = re.sub(r'\d+', "", sentence)

        sentence = re.sub(r"[/.,;`<>\+\-\*\?\'\’]", "", sentence)

        sentence = re.sub(r'\s+', " ", sentence)
        if common_words_list:
            common_set = set(common_words_list)
        else:   
            common_set = set()
        mytokens = [word.strip() for word in sentence.split(" ") if word not in common_set]
        return " ".join(mytokens).strip()
    
    @staticmethod
    def clean_receipt(df, column):
        """Removes empty or noisy lines from the dataframe, based on the content of a single column.

        Args:
            df (pandas.DataFrame): Dataframe containing the lines from a receipt.
            column (string): Name of the column to be used.

        Returns:
            pandas.DataFrame: Cleaned DataFrame.
        """
        # Strip blank spaces from all the columns
        df[column] = df[column].str.strip()
        # Drop rows with missing values 
        logging.debug('Before dropping rows with missing values: %d rows' % len(df))
        df[column] = df[column].replace('', np.nan)
        df = df.dropna(subset = [column], how='any')
        logging.debug('After dropping rows with missing values: %d rows' % len(df))
        # Convert all description to lowercase
        df[column] = df[column].str.lower()

        return df

    @staticmethod
    def filter_rare_items(df, label, remove_limit=5):
        """Performs further text cleaning to aid the NLP classification.

        Filters out items, whose class does not occur enough to be properly
        trained on.

        Args:
            df (pandas.DataFrame): DataFrame of input/label data.
            label (string): Column name of the labels.
            remove_limit (int, optional): [description]. Defaults to 5.

        Returns:
            [type]: [description]
        """
        # Filter to keep only items where there are more than k instances
        occurences = pd.DataFrame(df[label].value_counts().reset_index())
        occurences.columns = [label, 'count']
        df = pd.merge(df, occurences, on=label)
        df_rare = df[df['count'] < remove_limit].reset_index(drop=True)
        df = df[df['count'] >= remove_limit].reset_index(drop=True)
        # Drop column 'count'
        del df['count']
        del df_rare['count']
        return df, df_rare
