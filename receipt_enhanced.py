from receipt_raw import ReceiptRaw
from receipt_processed import ReceiptProcessed

import numpy as np
import cv2

from matplotlib import pyplot as plt

class ReceiptEnhanced(ReceiptProcessed):
    """Improves brightness and contrast for a previously projected image.

    Args:
        ReceiptProcessed (ReceiptProcessed): Processed ReceiptRaw
    """

    def __init__(self, child : ReceiptProcessed, normalised_gray_image, contrasted_image, contrast_result=True):
        super().__init__(child.raw_receipt, 
            child.scale_height, child.scale_width,
            child.projected_image, child.dilated_mask, child.approximated_mask)
        self.normalised_gray = normalised_gray_image
        self.contrasted = contrasted_image    
        self.contrast_result = contrast_result

    @property
    def ocr_image(self):
        if self.contrast_result:
            return self.contrasted
        else:
            return self.normalised_gray

    @classmethod
    def from_raw_receipt(cls, raw_receipt: ReceiptRaw, model, img_height=512, img_width=0, use_hough=True, 
                        skip_projection=False, ignore_projection_errors=False, contrast_result=True, verbose=False):
        processed_receipt = ReceiptProcessed.from_raw_receipt(raw_receipt, model, img_height, img_width, use_hough, skip_projection, ignore_projection_errors, verbose)
        return cls.from_processed_receipt(processed_receipt, contrast_result)

    @classmethod
    def from_processed_receipt(cls, processed_receipt:ReceiptProcessed, contrast_result=True):
        normalised_gray, contrasted = ReceiptEnhanced.improve_receipt_quality(processed_receipt.projected_image)
        return cls(processed_receipt, normalised_gray, contrasted, contrast_result)

    @staticmethod
    def improve_receipt_quality(src_image):
        # Dilate blobs to make text stand out more. (sadly noise will also become slightly larger)
        dilated_img = cv2.dilate(src_image, np.ones((7,7), np.uint8))

        # get a background by blurring text further.
        # Median blur is used to try focus on the white background which is more prominent
        # than the black text.
        bg_img = cv2.medianBlur(dilated_img, 5)

        #Calculate the difference between the original and the background we just obtained.
        #The bits that are identical will be black (close to 0 difference),
        #the text will be white (large difference).
        #Since we want receipts to be black on white, we invert the result.
        diff_img = 255 - cv2.absdiff(src_image, bg_img)

        # Normalize the image to be in the 0-255 range again.
        norm_img = diff_img.copy() # Needed for 3.x compatibility
        norm_gray = cv2.normalize(diff_img, norm_img, alpha=0, beta=255,
                                    norm_type=cv2.NORM_MINMAX,
                                    dtype=cv2.CV_8UC1)

        contrasted, _, _ = ReceiptEnhanced._auto_brightness_and_contrast(norm_gray.copy())
        return norm_gray, contrasted
    
    @staticmethod
    # Automatic brightness and contrast optimization with optional histogram clipping
    def _auto_brightness_and_contrast(image, clip_hist_percent=1, show_histogram=False):
        ## https://stackoverflow.com/questions/56905592/automatic-contrast-and-brightness-adjustment-of-a-color-photo-of-a-sheet-of-pape
        if len(image.shape) > 2:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            gray = image

        # Calculate grayscale histogram
        hist = cv2.calcHist([gray],[0],None,[256],[0,256])
        hist_size = len(hist)

        # Calculate cumulative distribution from the histogram
        accumulator = []
        accumulator.append(float(hist[0]))
        for index in range(1, hist_size):
            accumulator.append(accumulator[index -1] + float(hist[index]))

        # Locate points to clip
        maximum = accumulator[-1]
        clip_hist_percent *= (maximum/100.0)
        clip_hist_percent /= 2.0

        # Locate left cut
        minimum_gray = 0
        while accumulator[minimum_gray] < clip_hist_percent:
            minimum_gray += 1

        # Locate right cut
        maximum_gray = hist_size -1
        while accumulator[maximum_gray] >= (maximum - clip_hist_percent):
            maximum_gray -= 1

        # Calculate alpha and beta values
        alpha = 255 / (maximum_gray - minimum_gray)
        beta = -minimum_gray * alpha

        if show_histogram:
            # Calculate new histogram with desired range and show histogram
            new_hist = cv2.calcHist([gray],[0],None,[256],[minimum_gray,maximum_gray])
            plt.plot(hist)
            plt.plot(new_hist)
            plt.xlim([0,256])
            plt.show()

        auto_result = ReceiptEnhanced._convert_scale(image, alpha=alpha, beta=beta)
        return (auto_result, alpha, beta)

    @staticmethod
    def _convert_scale(img, alpha, beta):
        """Add bias and gain to an image with saturation arithmetics. Unlike
        cv2.convertScaleAbs, it does not take an absolute value, which would lead to
        nonsensical results (e.g., a pixel at 44 with alpha = 3 and beta = -210
        becomes 78 with OpenCV, when in fact it should become 0).
        """

        new_img = img * alpha + beta
        new_img[new_img < 0] = 0
        new_img[new_img > 255] = 255
        return new_img.astype(np.uint8)