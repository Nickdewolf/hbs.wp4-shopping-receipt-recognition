import base64
import cv2
import numpy as np

from receipt_raw import ReceiptRaw

class ReceiptRawServer(ReceiptRaw):
    """ 
    Adds request data from the CBS server in addition to the gray-scale and rgb image.
    """

    def __init__(self, rgb_image, gray_image, sync_id, sync_time, request_content):
        super().__init__(rgb_image, gray_image)
        self.sync_id = sync_id
        self.sync_time = sync_time
        self.request_content = request_content

    @classmethod
    def from_server_request(cls, sync_id, sync_time, request_content):
        rgb_image = cls._load_base64(request_content["image"]["base64image"])
        gray_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2GRAY)
        return(cls(rgb_image, gray_image, sync_id, sync_time, request_content))

    @staticmethod
    def _load_base64(base64_image):
        """ Load an image based on a base64 string representation,

        Args:
            base64_image (_type_): base64 version of an RGB image.

        Returns:
            _type_: _description_
        """
        nparr = np.frombuffer(base64.b64decode(base64_image), np.uint8)
        color_image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        return color_image
