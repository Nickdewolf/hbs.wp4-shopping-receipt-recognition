import os

from sklearn.base import BaseEstimator


class UnityExtractor(BaseEstimator):

    def __init__(self, input = 'content', encoding='utf-8'):
        self.input = input
        self.encoding = encoding


    def fit(self, text):
        return text

    def fit_transform(self, text, y=None):
        return text

    def transform(self, text):
        return text

    def inverse_transform(self, text):
        return text

    #def get_features_names
        