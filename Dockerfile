# syntax=docker/dockerfile:1
FROM python:3.9-slim
WORKDIR /code
COPY requirements.txt requirements.txt
RUN apt-get update \
    && apt-get install g++ cmake python3-dev libgl1-mesa-glx ffmpeg libsm6 libxext6 python3-opencv -y
ENV LIBRARY_PATH=/lib:/usr/lib
ENV CUDA_VISIBLE_DEVICES='-1'
RUN python -m pip install --upgrade pip
# pytesseract breaks during install if not installed
RUN python -m pip install wheel Pillow
RUN python -m pip install Cython
RUN python -m pip install -r requirements.txt
COPY . .
