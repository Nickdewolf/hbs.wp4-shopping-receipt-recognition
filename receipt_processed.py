from receipt_raw import ReceiptRaw

import numpy as np
import cv2

from matplotlib import pyplot as plt
from sklearn.cluster import KMeans

from scipy.spatial import distance as dist

import logging

class NoReceiptError(Exception):
    """Raised when the image segmentation does not find a receipt."""
    pass

class ReceiptProcessed():
    """A correctly project image of the receipt, as well as the raw data on which this projection was based. 
    """

    def __init__(self, child : ReceiptRaw, scale_height, scale_width, projected_image, dilated_mask, approximated_mask):
        self.raw_receipt = child
        self.scale_height = scale_height
        self.scale_width = scale_width
        self.projected_image = projected_image
        self.dilated_mask = dilated_mask   
        self.approximated_mask = approximated_mask   

    @property
    def rgb_image(self):
        """Retrieve RGB image"""
        return self.raw_receipt.rgb_image

    @property
    def gray_image(self):
        """Retrieve gray-scale image"""
        return self.raw_receipt.gray_image

    @property
    def ocr_image(self):
        return self.projected_image


    @classmethod
    def from_raw_receipt(cls, raw_receipt: ReceiptRaw, model, img_height=512, img_width=0, 
                        use_hough=True, skip_projection=False, ignore_projection_errors=False, verbose=False):
        gray_image = raw_receipt.gray_image
        if skip_projection:
            projected_image = gray_image
            dilated_mask = np.ones(gray_image.shape).astype(np.uint8) * 255
            approximated_mask = None
        else:
            projected_image, dilated_mask, approximated_mask = cls.auto_image_projection(
                model, 
                raw_receipt.rgb_image,
                raw_receipt.gray_image,
                img_height, 
                img_width, 
                use_hough=use_hough, 
                show_image=False, 
                verbose=verbose)
            
            if projected_image is None:
                if ignore_projection_errors:
                    projected_image = gray_image
                    dilated_mask = np.ones(gray_image.shape).astype(np.uint8) * 255
                else:
                    raise NoReceiptError('Could not detect the receipt boundaries properly.')
        return cls(raw_receipt, img_height, img_width, projected_image, dilated_mask, approximated_mask)

    @staticmethod
    def rescale_image(image, height=0, width=0):
        """
            Return rescaled image based on provided maximum width and height.
            If only one is provided, the scaling ratio is copied for the parameter that is zero.
        """
        # Image ratio must be higher than zero
        if height <= 0 and width <= 0:
            return image, (1,1)
        # Get the resize factors
        height_ratio = height / image.shape[0]
        width_ratio = width / image.shape[1]
        # If only one length is given, match the other direction
        if width == 0:
            width_ratio = height_ratio
        elif height == 0:
            height_ratio = width_ratio
        # Get the dimension of the resized image.
        width = int(image.shape[1] * width_ratio)
        height = int(image.shape[0] * height_ratio)
        dim = (width, height)
        resized = cv2.resize(image, dim, interpolation = cv2.INTER_LANCZOS4 )
        return resized, (width_ratio, height_ratio)

    @staticmethod
    def _get_receipt_mask(dl_result):
        """
            Select the mask corresponding with the receipt from the
            list of objects found by object detection. They are sorted from
            largest to smallest, hence we pick the first because
            we assume the receipt is the largest object in the image.
        """
        if len(dl_result) < 1:
            return None
        masks = dl_result[0]["masks"]
        if masks.shape[2] == 0:
            return None

        # From a 3-d numpy array, retrieve the layer that contains the largest mask.
        # Masks are 2-d arrays with values being either 0 or 1.
        # Hence the "largest mask" is determined summing the values in the 2-d array.
        biggest_mask_index = np.argmax(sum(sum(masks)))
        return masks[:, :, biggest_mask_index]

    @staticmethod
    def _naive_split_horizontal_vertical(lines):
        """
            Lines is a list of tuples containing (a, b, c) where: ax + by + c = 0
        """
        hor_lines = []
        ver_lines = []

        for (a, b, c) in lines:
            if abs(b) > abs(a):
                hor_lines.append((a,b,c))
            else:
                ver_lines.append((a, b, c))
        return hor_lines, ver_lines

    ## Intersection code
    @staticmethod
    def _get_line_formula(x1, y1, x2, y2):
        """
            Generate a, b, c to represent the lines between point (x1,y1) and (x2, y2)
            cross( (x1,y1,1), (x2, y2, 1)) = (a, b, c)
        """
        h1 = [x1, y1, 1]
        h2 = [x2, y2, 1]
        (a, b, c) = np.cross(h1, h2)
        return a, b, c

    @staticmethod
    def _get_line_intersection(line1, line2):
        """
            Linear algebra explanation using the cross product between 1 line definitions:
            cross( (a1, b1, c1), (a2, b2, c2)) = (x, y, W) == intersection (x/W, y/W)
        """
        h_intersect = np.cross(line1, line2)
        (x, y) = (h_intersect[0] / h_intersect[2], h_intersect[1] / h_intersect[2])
        return x, y

    @staticmethod
    def _get_intersections(hor_lines, ver_lines):
        """
            Get the intersection points between lines stored in an (a, b, c) notation.
            Output is a list of tuples containing (x,y) tuples.
        """
        intersections = []
        for hor_line in hor_lines:
            for ver_line in ver_lines:
                intersections.append(ReceiptProcessed._get_line_intersection(hor_line, ver_line))
        return intersections

    @staticmethod
    def _get_corners_from_houghlines(contour_mask):
        """
            Find lines in contour mask image
        """
        lines = cv2.HoughLinesP(contour_mask, 1, np.pi/180, 10, minLineLength=50, maxLineGap=30)
        lines_form = []
        # Write them in line-form (a, b, c) -> ax + by + c
        for line in lines:
            x1, y1, x2, y2 = line[0]
            li = ReceiptProcessed._get_line_formula(x1, y1, x2, y2)
            lines_form.append(li)

        # Split between horizontal and vertical lines
        hor_lines, ver_lines = ReceiptProcessed._naive_split_horizontal_vertical(lines_form)
        # Get the interesctions between these lines
        intersections = ReceiptProcessed._get_intersections(hor_lines, ver_lines)
        # Cluster the interesctions to get 4 points
        kmeans = KMeans(n_clusters=4, random_state=0).fit(intersections)
        centers = np.array(ReceiptProcessed._order_points(kmeans.cluster_centers_), np.int32)
        return centers

    @staticmethod
    def detect_contours_reduce(state, use_hough=True, plot_results=False):
        """
            Detect contours in the image. And reduce these to 4 intersection points that can be used
            to apply an affine transformation to the image. (receipt straightening)
        """
        contours, hierarchy = cv2.findContours(state, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        ## Mask for all contours
        all_mask = np.zeros(state.shape, np.uint8)
        cv2.drawContours( all_mask, contours, -1, (255,255,255), 1, cv2.LINE_AA, hierarchy )

        ## Mask for largest contour
        # Currently we only want the largest one.
        contour_sizes = [(cv2.contourArea(contour), i) for i, contour in enumerate(contours)]
        largest_contour = max(contour_sizes, key=lambda x: x[0])[1]

        mask = np.zeros(state.shape, np.uint8)
        cv2.drawContours(mask, contours, largest_contour, (255,255,255), 8, cv2.LINE_AA, hierarchy)

        if use_hough:
            approx = ReceiptProcessed._get_corners_from_houghlines(mask)
            approx = np.reshape(approx, (4, 1, 2))
        else:
            ## Final approximation
            # Aproximate shortest lines. The higher the number, the less lines will be used.
            # 0.1 or lower seems to improve results.
            # 0.05 = works most
            # 0.01 = example12
            epsilon = 0.05*cv2.arcLength(contours[largest_contour],True)
            approx = cv2.approxPolyDP(contours[largest_contour],epsilon,True)

            additional_options = [0.1, 0.05, 0.025, 0.01, 0.005]
            while len(approx) < 4 and len(additional_options) > 0:
                logging.debug("Only found {0:d} edges, reducing the Epsilon value.".format(len(approx)))
                epsilon = additional_options.pop(0)*cv2.arcLength(contours[largest_contour],True)
                approx = cv2.approxPolyDP(contours[largest_contour],epsilon,True)

        contour_mask = np.zeros(state.shape, np.uint8)
        cv2.drawContours( contour_mask, [approx], 0, (255,255,255),cv2.FILLED, 8)

        ## Plot the results
        if plot_results:
            fig, ax = plt.subplots(1,4)
            ax[0].imshow(state, "gray")
            ax[0].set_title('Original')
            ax[1].imshow(all_mask, "gray")
            ax[1].set_title('All contours')
            ax[2].imshow(mask, "gray")
            ax[2].set_title('Largest Contour')
            ax[3].imshow(contour_mask, "gray")
            ax[3].set_title('Final Approximation')
            plt.show()
        return contour_mask, approx[:, 0, :]    

    @staticmethod
    def auto_image_projection(mrcnn_model, color_image, gray_image, img_height=512, img_width=0, use_hough=True, show_image=False, verbose=False):

        resized_image, ratios = ReceiptProcessed.rescale_image(color_image, img_height, img_width)
        # Run the model and get the mask on the resized image.
        receipt_mask = ReceiptProcessed._get_receipt_mask(mrcnn_model.detect([resized_image], verbose=verbose))
        # Check if any mask was found or not, else skip
        if receipt_mask is None:
            return None, None

        # We have to convert the float representation to integer.
        # We use a new image for this.
        receipt_mask_uint8 = np.zeros(receipt_mask.shape, dtype = np.uint8)
        receipt_mask_uint8[:, :] = np.where(receipt_mask == 1, 255, 0)

        # The model always detects the insides of a receipt,
        # by adding 10 more pixels using dilation around the contour.
        # You fill any potential gaps while also getting slightly more of the receipt.
        # If the height of 512 is changed, the value of 10 should also be changed accordingly.
        dilated_mask = cv2.dilate(receipt_mask_uint8, np.ones((10, 10), np.uint8))

        _, approximated_mask = ReceiptProcessed.detect_contours_reduce(dilated_mask,
                                                            use_hough=use_hough,
                                                            plot_results=show_image)

        if len(approximated_mask) != 4:
            return None, None, None

        # Use the resized image to compute the homography matrix
        homography_matrix, dimensions = ReceiptProcessed._make_homography(approximated_mask)
        cols, rows = dimensions

        # Calculate the homography matrix for the original image by reverting the ratios by which it
        # was previously reduced
        s_matrix = np.diag([1 / ratios[1], 1 / ratios[0], 1])
        new_homography_matrix = s_matrix.dot(homography_matrix).dot(np.linalg.inv(s_matrix))

        # Project the image
        projected_image = cv2.warpPerspective(gray_image, new_homography_matrix,
                                        (int(cols/ratios[0]), int(rows/ratios[1])))

        return projected_image, dilated_mask, approximated_mask    

    @staticmethod
    def _make_homography(approx):
        """ Accepts an 2D array/list with the points sorted in a specific order

        Points have to be in a specific order that we determined as seen below.
        [[0, 0], [cols-1, 0], [cols-1, rows-1], [0, rows-1]]

        Args:
            approx (list): List of coordinates

        Returns:
            (numpy.array, (int, int)): Returns the homography matrix and the
                                       dimensions of the resulting image
        """
        # Restructure points from the approximation such that they can be easily sorted
        coords = np.array([x.flatten() for x in list(approx)])
        # Put the points in order
        points1 = ReceiptProcessed._order_points(coords)
        # Compute width and height of the resulting receipt .
        rows = int(np.linalg.norm(points1[0] - points1[3]))
        cols = int(np.linalg.norm(points1[0] - points1[1]))
        # Calculate the resulting homography matrix
        homography, _ = cv2.findHomography(points1,
            np.array([[0, 0], [cols-1, 0], [cols-1, rows-1], [0, rows-1]]))
        return homography, (cols, rows)

    @staticmethod
    def _order_points(corners):
        """Sorts the corners to always be in the same order

        Sort the corners in top-left, top-right, bottom-right, bottom-left
        List should have len(corners) == 4.

        Args:
            corners (list): List of coordinates

        Returns:
            list: Sorted list of coordinates
        """
        # sort the points based on their x-coordinates
        x_sorted = corners[np.argsort(corners[:, 0]), :]

        # grab the left-most and right-most points from the sorted
        # x-roodinate points
        left_most = x_sorted[:2, :]
        right_most = x_sorted[2:, :]

        # now, sort the left-most coordinates according to their
        # y-coordinates so we can grab the top-left and bottom-left
        # points, respectively
        left_most = left_most[np.argsort(left_most[:, 1]), :]
        (top_left, bottom_left) = left_most

        # now that we have the top-left coordinate, use it as an
        # anchor to calculate the Euclidean distance between the
        # top-left and right-most points; by the Pythagorean
        # theorem, the point with the largest distance will be
        # our bottom-right point
        distances = dist.cdist(top_left[np.newaxis], right_most, "euclidean")[0]
        (bottom_right, top_right) = right_most[np.argsort(distances)[::-1], :]

        # return the coordinates in top-left, top-right,
        # bottom-right, and bottom-left order
        return np.array([top_left, top_right, bottom_right, bottom_left])
