from difflib import get_close_matches
import os
import re
import pprint as pp
import logging 

#import nltk
#nltk.download('punkt')
from nltk.tokenize import word_tokenize
from nltk.util import ngrams

class Yaml2Dict:
    """Read the config file in yaml format and turn the content into a dictionary.
    """
    def __init__(self, d):
        self.__dict__ = d

def list_files_in_folder(data_folder, extensions):
    """List all files with the provided extensions in "data_folder" and return list of file names.
    """
    filenames = []
    for file in os.listdir(data_folder):
        if any(file.endswith(ext) for ext in extensions):
            filenames.append(data_folder + '/' + file)
    return filenames

def get_ngrams(line, max_n):
    """Produce 1-n-grams for a given line.
    """
    ngram_list = []
    for ngram_size in range(1, max_n):
        n_grams = ngrams(word_tokenize(line), ngram_size)
        ngram_list = ngram_list + [' '.join(grams) for grams in n_grams]
    return ngram_list

def fuzzy_match(pattern_list, lines, n_levels=1, max_acc_only=1):
    """Find words that closely resemble one of the patterns in a list of patterns
        Does not need to be an exact match. Two levels of dictionary supported
        https://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python/
        https://stackoverflow.com/questions/31806695/when-to-use-which-fuzz-function-to-compare-2-strings
    """
    found = []  # Default value if no pattern found
    idx = []
    accuracies = []
    logging.debug('Fuzzy Matching pattern list:')
    logging.debug(pattern_list)
    for int_accuracy in range(10, 7, -1):
        accuracy = int_accuracy / 10.0
        for i in range(0, len(lines)):
            logging.debug('Line %d: %s. Accuracy=%f' % (i, lines[i], accuracy))
            # Remove digits
            line = ''.join([i for i in lines[i] if not i.isdigit()])
            # Make word ngrams list
            my_ngrams = get_ngrams(line, 4)
            if n_levels == 1:
                for pattern in pattern_list:
                    # Fuzzy match by keyword (better!) or exact match by pattern
                    if get_close_matches(pattern, my_ngrams, 1, accuracy) or re.search(pattern, lines[i]):
                        # if not i in idx:  # if index not already in set
                        idx.insert(0, i)
                        accuracies.insert(0, accuracy)
                        found.insert(0, pattern)
                        logging.debug('\n\tLine index=%d, pattern: %s\n' % (i, pattern))
            elif n_levels == 2:
                for pattern, spellings in pattern_list:
                    for spelling in spellings:
                        if get_close_matches(spelling, my_ngrams, 1, accuracy) or re.search(spelling, lines[i]):
                            # if not i in idx:  # if index not already in set
                            idx.insert(0, i)
                            accuracies.insert(0, accuracy)
                            found.insert(0, pattern)
                            logging.debug('\n\tLine index=%d, pattern: %s, found: %s\n' % (i, spelling, pattern))
        if max_acc_only == 1 and len(idx) != 0:
            return [idx, accuracies, found]
    return [idx, accuracies, found]


# ------------------------------------------------------------------------------------ #
# All functions for plotting and printing results
# ------------------------------------------------------------------------------------ #

def print_raw_ocr(lines, print_pos=0, print_confidence=0):
    """Print raw Tesseract OCR output because any processing

        TODO: Add optional position and confidence values to print
    """
    print('\n\nOCR output before cleaning...')
    for line in lines:
        print(line)

def print_items(lines, head=-1):
    """Function for printing the first head rows of lines in list
    """
    if head == -1:
        head = len(lines)
    for i in range(0,head):
        print(lines[i])

def print_config(config_vars):
    """Print parameters in config yaml file e.g. known shops, keywords
    """
    pp.print(config_vars)
