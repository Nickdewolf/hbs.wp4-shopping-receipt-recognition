""" Functions involving data cleaning after OCR

Prior to parsing (take line_boxes as input, output list of lines
All functions used for cleaning prior to parsing

Written by Lanthao Benedikt (ONS), edited by Nick de Wolf (CBS)
"""
import re
import copy
import ocr_utils
import logging

def pre_process_ocr_output(lines_with_confidence, keyterms, verbose=False):
    """Pre-processing prior to parsing

    Args:
        lines (list): List of strings. Each entry is a detected line
        keyterms (list): List of words to look for to remove "garbage" lines
        verbose (boolean, optional): IPrint verbose if true.

    Returns:
        (list, list): The original lines and the processed lines
    """

    # Step 1: Extract text content from OCR line_boxes
    lines_with_confidence = extract_ocr_text(lines_with_confidence)

    # Step 2: Basic cleaning: lowercase, empty lines, reformat prices, remove noisy characters
    lines_with_confidence = basic_cleaning(lines_with_confidence, verbose)

    # Step 3: remove garbage lines by keywords. Keep a copy of the original content for parsing
    #         date, shopname and payment mode
    orig_lines = copy.deepcopy(lines_with_confidence)
    new_lines = remove_garbage_lines_by_keyword(lines_with_confidence, keyterms, verbose=verbose)

    if verbose:
        print('\nClean original OCR output:')
        ocr_utils.print_items(orig_lines)
        print('\nClean trimmed OCR output:')
        ocr_utils.print_items(new_lines)
    return orig_lines, new_lines

def extract_ocr_text(lines_with_confidence):
    """Extract non empty line content of line_boxes

    Args:
        lines_with_confidence (list): List of tuples of strings and scores

    Returns:
        list: List of strings without empty lines
    """
    new_lines = []
    for i, line in enumerate(lines_with_confidence):
        # Line has at least 2 characters
        if len(line[0].strip()) > 1: 
            new_lines.append(line)
        else:
            logging.debug(f"Removed line: {i}")
    return new_lines

def basic_cleaning(lines_with_confidence, verbose=False):
    """Basic cleaning: lowercase, empty lines, reformat prices, remove noisy characters.

    Args:
        lines (list): List of strings
        verbose (boolean, optional): Print verbose if true.

    Returns:
        [type]: [description]
    """
    if verbose:
        print('\nBasic cleaning (BEFORE)')
        ocr_utils.print_items(lines_with_confidence)
    new_lines = []
    for line in lines_with_confidence:
        new_line = line[0]
        new_line = new_line.lower()  # convert to lowercase
        new_line = ' '.join(new_line.split())  # collapse multiple spaces to single space
        new_line = reformat_prices(new_line)  # correct known errors in prices
        new_line = reformat_barcodes(new_line)  # correct known errors in prices
        new_line = remove_specchars(new_line)  # remove noisy special characters
        new_lines.append((new_line, line[1]))
    if verbose:
        print('\nBasic cleaning (AFTER)')
        ocr_utils.print_items(new_lines)
    return new_lines

def reformat_prices(line):
    """OCR of prices tend to go wrong in many ways, we have to correct it.

    Args:
        line (string): String representation of a price.

    Returns:
        string: Cleaned string that can be parsed by float().
    """

    # convert french/dutch price separator to english format
    line = line.replace(',', '.')
    # Sometimes, price in Superstore have unwanted space e.g. 3. 48 instead of 3.48
    if re.search(r'\d{1,3}. \d{1,2}$', line) or re.search(r'\d{1,3} .\d{1,2}$', line):
        line = line.replace(' .', '.')
        line = line.replace('. ', '.')
    # Some supermarkets have codes on the right handside of prices, that needs to
    # be removed. Check if last word in line is know garbage keyword
    words = line.split()
    if len(words) >= 2:  # Otherwise, we can't take the last 2 words
        # Check if before last word has a price structure and last word is a letter or 'FP'
        if words[-1].isalpha() and re.search(r'\d{1,3}.\d{1,2}', words[-2]):
            # FP is known code after prices, Remove last word if it's a letter
            if len(words[-1]) == 1 or words[-1] in ['fp', 'hc']:
                words = words[:-1]
            # Remove dollar sign in front of price to be consistent
            # TODO: Use currencies from config file.
            words[-1] = words[-1].replace('$', '')
            # between supermarkets (Walmart has $, other shops not)
    line = ' '.join(words)
    return line

def reformat_barcodes(line):
    """Some Barcodes have a letter at the end: remove that to be consistent with other shops.

    Mostly used for UK receipts.

    Args:
        line (string): Remove a letter behind barcodes.
    Returns:
        string: Original string with possibly corrected barcode
    """
    words = line.split()
    for i, word in enumerate(words):  # Assumption barcodes have between 4 and 14 digits
        if re.search(r'\d{4,14}', word):
            words[i] = words[i].replace('r', '')
    line = ' '.join(words)
    return line

def remove_specchars(line):
    """Remove special characters that tend to be noise and specific words that mess things up.

    TODO: In the future, this can be done with Tesseract blacklist (not supported in version 4.0).

    Args:
        line (string): Line of detected text.

    Returns:
        string: Cleaned string
    """
    noisy_chars = r'“!"*,\^_`{|}~«=°‘;'
    for char in line:
        if char in noisy_chars:
            line = line.replace(char, '').strip()
    return line

def remove_garbage_lines_by_keyword(lines_with_confidence, keywords, verbose=False):
    """Function for removing garbage lines based on keywords.

    Specific words are usually specified in configs/ocr_config.yml.

    Args:
        lines (list): List of strings with detected lines in receipt
        keywords (list): List of unwanted words
        verbose (boolean, optional): Print verbose if true.

    Returns:
        list: Initial list without lines that includes one of the keywords.
    """
    patterns = keywords
    lines = [line[0] for line in lines_with_confidence]
    logging.debug("Removing garbage lines by keyword:")
    [line_ids, _, _] = ocr_utils.fuzzy_match(patterns, lines,
                                                     n_levels=1,
                                                     max_acc_only=0)                                                     
    # Remove lines that contain garbage words (as defined in ocr_config)
    for line_id in sorted(set(line_ids), reverse=True):
        lines_with_confidence.pop(line_id)
    if verbose:
        print('\nAfter remove garbage lines by keywords')
        ocr_utils.print_items(lines_with_confidence)
    return lines_with_confidence

# ------------------------------------------------------------------------------------ #
# POST-PROCESSING: After parsing (take df series as input, output df series)
#                  All functions for cleaning item descriptions after parsing
# ------------------------------------------------------------------------------------ #
#
def post_process_parsed_df(input_df, column_name):
    """Pre-processing prior to parsing

    Args:
        df (pandas.DataFrame): pandas.DataFrame after parsing run
        column_name (string): Column name to look for the keywords
        keyterms (list): Terms to look for to find the header of the receipt

    Returns:
        pandas.DataFrame: Processed dataFrame
    """
    if len(input_df) > 0:
        # Remove total, subtotal, hst, etc.
        idx = remove_total(input_df[column_name])
        input_df = input_df.drop(input_df.index[idx])

        # If description is empty, drop the row
        input_df.dropna(subset=[column_name], inplace=True)
    return input_df

def remove_total(df_series):
    """Function for removing sub-total, subtotal

    Args:
        df_series (pandas.Series): Series of detected lines in receipt

    Returns:
        list: List of indices of lines mentioning total/subtotal.
    """
    lines = list(df_series)
    patterns = ['total', 'totale', 'sous-totale', 'sub-total', 'subtotal', 'subtotaal', 'hst']
    logging.debug("Removing sub-total and total price.")
    [line_ids, _, _] = ocr_utils.fuzzy_match(patterns, lines,
                                                     n_levels=1,
                                                     max_acc_only=0)
    return line_ids
