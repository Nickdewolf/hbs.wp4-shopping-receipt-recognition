7# Optical Character Recognition and Machine Learning Classification of Shopping Receipts

This is an collaborative implementation between:
* Division of Methodology and Quality, Statistics Netherlands, The Netherlands
* Data Science Campus, Office for National Statistics, United Kingdom

The repository includes:
* Source code of the OCR implementation
* Source code of the image processing on mobile photos
* Pre-trained weights for the retrained M-RCNN model for mask generation
* Coicop training and classification code
* Report for HBS WP4 project number ESSnet SEP-2105369

The code is still a work-in-progress as far as documentation goes, but it should provide insight into the underlying steps taken to get the results presented in the report.
Where possible the code has been seperated into different classes and functions as to make the code easy to extend and apply elsewhere.

The code is divided into 4 different categories, namely:
1. `data collection`: A simple interface for retrieving and storing receipts. `cbs_server_collection` is the implementation as used by the CBS backend.
2. `api/request`: Code that handles the communication with the backend REST api. 
3. `image processing`: Code that involves image processing/segmentation. This is stored per receipt, and can be recognised by python files starting with `receipt`.
4. `ocr`: Code that involves OCR and Natural Language Processing of the images.
5. `coicop`: Code that involves the training of COICOP classifiers, and classying product description into their corresponding COICOP code.

## Requirements
1. [Tesseract](https://tesseract-ocr.github.io/tessdoc/) (v4.0.0 or higher)
2. [Tessdata](https://tesseract-ocr.github.io/tessdoc/Data-Files.html) (for the pretrained language-models you wish to use)
2. Python 3.9 and the packages listed in `requirements.txt`.

## Installation
1. Clone this repository
2. (Optional) Clone and install [MRCNN](https://github.com/matterport/Mask_RCNN) (if the receipt segmentation does not require retraining, the already included mrcnn folder should suffice). Do note that the included mrcnn folder is upgraded to work with Tensorflow 2.+. Adjustments to the code were based on [this](https://github.com/Kamlesh364/Mask-RCNN-TF2.7.0-keras2.8.0) fork.
3. Install dependencies
   ```bash
   pip3 install wheel Pillow
   pip3 install -r requirements.txt
   ```
4. Download pre-trained weights (`mrcnn_model.7z` and `coicop_models.7z`) from the [releases page](https://gitlab.com/Nickdewolf/hbs.wp4-shopping-receipt-recognition/-/releases). Place/Unpack them in the root folder. The mrcnn_model folder should be directly under the root.
The current COICOP model is trained to classify 122 different COICOP codes. The specific COICOP codes and their dutch names can be found in `CoicopNr-Naam.csv` (Model only works in Dutch at the moment).
5. Open python in a terminal and type `import nltk` followed by `nltk.download('punkt')` (Used in tokenization during parsing).

## Examples
Examples of the various parts of the code can be found in the included jupyter notebooks:
* `coicop_data` : Includes samples of the data used to train the COICOP classification model. The `_sample.csv` files are only intended to show the type of data in each of the files as used in `00_Prepare_CBS_data.ipynb`. The files in this folder illustrate the way CBS handled the data from various sources, this process is not mandatory. The actual training uses the resulting, preprocessed file as can be found in `coicop_input`.
* `coicop_input` : Includes samples of the data that would have been the result from running `00_Prepare_CBS_data.ipynb` successfully. The files in this folder are used in `01_Train_Models.ipynb`. Using the `_sample.csv` files will not result in a proper model, and it might even complain about having too few examples of each of the classes. Your own data has to be gathered.
* `00_Prepare_CBS_data.ipynb`: Includes the code used to prepare the CBS data, which consisted of:
   1. Supermarket scanner data where only the product description and COICOP code/name were kept.
   2. Data collected from previous testruns of the app, once again only storing product descriptions and COICOP codes/names
   3. Supermarket data where product descriptions as shown on the receipt, were translated to a more human readable product description.
* `01_Train_Models.ipynb`: Includes the code to train models for the COICOP classification. In order to try different combinations of settings the `coicop_classifier.py` file has to be editted.
* `02_Test_pipeline.ipynb`: Contains the code, to communicate with the backend server and process the images. This file was mostly used for debugging, where the seperate parts of the pipeline can be run in seperate blocks to investigate the various intermediate steps.
* `03_Tune_Models.ipynb`: Contains a hyperparameter optimization for the COICOP classfiication. It currently tunes the hyperparameters of a FastText model, but it can easily be adapted to other models. It uses sklearn's GridSearch or RandomizedSearch. The possible models are specified in the variable `self.dict_clf` from `coicop_classifier.py`.

## Run
A file has also been included to automatically communicate with the backend server, after which each entry that includes an image will be processed, and the results returned to the server.
```bash
python main.py -r configs/request_config.json -o configs/ocr_config.yml -m mask_rcnn_receipt.h5 -n FastText_ngram.bin -e Label_Encoder.bin -c CoicopNr-Naam.csv
```

By typing the help command various other parameters can be set, such as the time to wait between calling the server for new receipts.
```bash
python main.py --help
```

Another way to run de code, assuming all the installation steps have been executed is to use the added Dockerfile.
The -u parameter is added such that the print statements in python are directly shown in the terminal:
```bash
docker build --tag hbs_backend .
docker run hbs_backend python -u main.py -r configs/request_config.json -o configs/ocr_config.yml -m mask_rcnn_receipt.h5 -n FastText_ngram.bin -e Label_Encoder.bin -c CoicopNr-Naam.csv
```
